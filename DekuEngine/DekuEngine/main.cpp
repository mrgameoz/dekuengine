
// DekuEngine.cpp : main project file.
#include "EngineCore.h"
#include "TestApplication.h"
int main(int argc, char* argv[]) 
{
	TestApplication game;
	if (!game.InitApplication(1280, 720))
	{
		EDebug::LogError("Failed to initialize!");
	}
	else
	{
		game.LoadScene();
		while (game.IsApplicationRunning())
		{
			game.Run();
		}
	}
	game.Clear();
	return 0;
}
