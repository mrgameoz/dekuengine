#include "TestAnimator.h"


TestAnimator::TestAnimator(bool playOnAwake):EAnimationController::EAnimationController(playOnAwake)
{
	idle = new EAnimationClip("Idle",1, 32, 0, 32, 32, true);
	walkL = new EAnimationClip("WalkL",3, 0, 32, 32, 32, true);
	walkR = new EAnimationClip("WalkR", 3, 0, 64, 32, 32, true);
	walkF = new EAnimationClip("WalkF", 3, 0, 0, 32, 32, true);
	walkB = new EAnimationClip("WalkB", 3, 0, 96, 32, 32, true);
	currentClip = SwitchToClip(idle);
}

TestAnimator::~TestAnimator()
{
	//clear all memory
	delete idle;
	idle = nullptr;
	delete walkL;
	walkL = nullptr;
	delete walkR;
	walkR = nullptr;
	delete walkF;
	walkF = nullptr;
	delete walkB;
	walkB = nullptr;
}


void TestAnimator::AnimatorFlow()
{
	//define state and animations for states
	if (animationState == state::state_idle)
	{
		if (!currentClip->CompareName("Idle"))
		{
			currentClip = SwitchToClip(idle);
		}
	}
	if (animationState == state::state_walkL)
	{
		if (!currentClip->CompareName("WalkL"))
		{
			currentClip = SwitchToClip(walkL);
		}
	}
	if (animationState == state::state_walkR)
	{
		if (!currentClip->CompareName("WalkR"))
		{
			currentClip = SwitchToClip(walkR);
		}
	}
	if (animationState == state::state_walkF)
	{
		if (!currentClip->CompareName("WalkF"))
		{
			currentClip = SwitchToClip(walkF);
		}
	}
	if (animationState == state::state_walkB)
	{
		if (!currentClip->CompareName("WalkB"))
		{
			currentClip = SwitchToClip(walkB);
		}
	}
}

void TestAnimator::SetState(state val)
{
	animationState = val;
}
