#pragma once
#include "EngineCore.h"
#include "TestAnimator.h"

class TestPlayer : public EActor
{
public:
	TestPlayer();
	~TestPlayer();
	void Initialize() override;
	void Update(float dt) override;
	void InputHandler() override;
	TestAnimator* playeranim;
	EAudioChunkComponent* chunk;
};

