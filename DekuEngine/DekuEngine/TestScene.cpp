#include "TestScene.h"

TestScene::TestScene(string name) :EScene(name)
{
	player = new TestPlayer();
	dekuimage = new EActor();
	fpsText = new EUIText();
	helpText = new EUIText();
	exitButton = new EUIButton(EVector3F(0.85f,0.05f));
	bgm = new EAudioMusicComponent("assets/bgm.mp3", true);
}


TestScene::~TestScene()
{
	//delete bgm after use
	if (bgm != nullptr)
	{
		delete bgm;
		bgm = nullptr;
	}
}


void TestScene::Initialize()
{
	EScene::Initialize();
	//world objects
	dekuimage->name = "bg";
	dekuimage->SetSpriteFromFile("assets/deku.png");
	dekuimage->transform.position = EVector3F(EWindow::SCREEN_WIDTH/2, EWindow::SCREEN_HEIGHT / 2);//values in world space 
	dekuimage->GetTexture()->SetPivotPoint(EVector3F(0.5f, 0.5f));
	dekuimage->renderOrder = 0;
	player->name = "player";
	player->SetSpriteFromFile("assets/People1.png");
	player->transform.position = EVector3F(0, 0);
	player->renderOrder = 1;
	AddInputObjectsToScene(player);
	AddGameObjectToScene(dekuimage);
	AddGameObjectToScene(player);

	//ui objects
	helpText->GetTextComponent()->SetText("Press ASWD to move player");
	helpText->transform.position = EVector3F(0.05f, 0.95f);//values in screen space(always range from 0.0f to 1.0f)
	helpText->GetTextComponent()->SetColor(SDL_Color{ 255, 255, 255, 255 });
	helpText->GetTextComponent()->SetPivotPoint(EVector3F(0, 0));

	//fps text
	fpsText->transform.position = EVector3F(0.90f, 0.95f);
	fpsText->GetTextComponent()->SetColor(SDL_Color{ 255, 255, 255, 255 });
	fpsText->GetTextComponent()->SetPivotPoint(EVector3F(0, 0));

	//button
	exitButton->SetImageForState("assets/b1.png",1);
	exitButton->SetImageForState("assets/b2.png", 2);
	exitButton->SetImageForState("assets/b3.png", 3);
	exitButton->text->transform.position = exitButton->images[0]->GetTexture()->GetMidPointOnScreen();
	exitButton->text->GetTextComponent()->SetPivotPoint(EVector3F(0.5f, 0.5f));
	exitButton->text->GetTextComponent()->SetText("Exit");


	//add to controller
	uiController->AddUIComponents(helpText);
	uiController->AddUIComponents(fpsText);
	uiController->AddUIComponents(exitButton);

	//play bgm
	bgm->Play();
	message_callbackid = ECallbackAction::CreateCallbackAction(2000, callbackPointer, this);//create a callback
	
}

void TestScene::InputHandler()
{
	EScene::InputHandler();
	if (EInput::GetKeyDown(KeyCode::KEY_1))
	{
		UnloadScene();
	}
	if (EInput::GetKeyUp(KeyCode::KEY_q))
	{
		EApplication::QuitApplication();
	}
	if (EInput::GetKeyUp(KeyCode::KEY_c))
	{
		ECallbackAction::RemoveCallbackAction(message_callbackid);//press c to remove callback action
	}
	if (exitButton->OnMouseUp())
	{
		EApplication::QuitApplication();
	}

}

void TestScene::Update(float dt) 
{
	EScene::Update(dt);
	timer += dt;
	//update fps text
	fpsText->GetTextComponent()->SetText("FPS: "+to_string((int)EUtil::GetFPS()));
}

Uint32 TestScene::callbackPointer(Uint32 interval, void* param)
{
	
	return reinterpret_cast<TestScene*>(param)->message(interval, "hello");
}


Uint32 TestScene::message(Uint32 interval, void* param)
{

	printf("Callback called back with message: %s\n", (char*)param);
	return 0;

}
