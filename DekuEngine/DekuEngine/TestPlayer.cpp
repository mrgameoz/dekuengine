#include "TestPlayer.h"


TestPlayer::TestPlayer()
{
	playeranim = new TestAnimator(true);
	animationController = playeranim;
	chunk = new EAudioChunkComponent("assets/magic.mp3", true);
}

TestPlayer::~TestPlayer()
{
	if (chunk != nullptr)
	{
		delete chunk;
		chunk = nullptr;
	}
}

void TestPlayer::Initialize()
{
	EActor::Initialize();

}

void TestPlayer::InputHandler()
{
	EActor::InputHandler();
	if (EInput::GetKeyPress(KeyCode::KEY_d))
	{
		transform.Translate(EVector3F(1.0f, 0.0f));//move player
		playeranim->SetState(playeranim->state_walkR);//set player animation state
	}
	else if (EInput::GetKeyPress(KeyCode::KEY_a))
	{
		transform.Translate(EVector3F(-1.0f, 0.0f));
		playeranim->SetState(playeranim->state_walkL);
	}
	else if (EInput::GetKeyPress(KeyCode::KEY_w))
	{
		transform.Translate(EVector3F(0.0f, -1.0f));
		playeranim->SetState(playeranim->state_walkB);
	}
	else if (EInput::GetKeyPress(KeyCode::KEY_s))
	{
		transform.Translate(EVector3F(0.0f, 1.0f));
		playeranim->SetState(playeranim->state_walkF);
	}
	else
	{
		playeranim->SetState(playeranim->state_idle);
	}


}


void TestPlayer::Update(float dt)
{
	EActor::Update(dt);	
}




