#pragma once
#ifndef EWINDOW_H
#define EWINDOW_H
#include <SDL.h>
#include <stdio.h>
#include "EDebug.h"
#include "EUtil.h"

using namespace std;
class EWindow {

private:
	//The window we'll be rendering to
	SDL_Window* gWindow = NULL;
	//The window renderer
	static SDL_Renderer* gRenderer;

public:
	static int SCREEN_WIDTH;
	static int SCREEN_HEIGHT;
	static int DESIGN_WIDTH;
	static int DESIGN_HEIGHT;
	//scale x axis difference of screen compared to design 
	static float xScreenScaleDiff;
	//scale y axis difference of screen compared to design 
	static float yScreenScaleDiff;

public:
	//create window with screen width and height, if design w/h is not assigned it will take screen w/h 
	EWindow(int screenWidth, int screenHeight,int designwidth=0,int designheight=0);
	//
	~EWindow();
	//get renderer of the window
	static SDL_Renderer* GetRenderer();
	//set design size
	void SetDesignSize(int width, int height);
	//set screen size
	void SetScreenSize(int width, int height);
	//initialize window
	virtual bool InitWindow();
	//free window
	virtual void Free();

private:
	//set the scale value for x/y difference axis
	void SetScaleSizes();
	//recorrect screen/scales size if the screen size set is bigger than the monitor size
	void RecorrectWindowSize();
};

#endif // !EWINDOW_H
