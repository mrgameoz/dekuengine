#include "EAudioChunkComponent.h"


EAudioChunkComponent::EAudioChunkComponent(string path,bool playOnAwake, int vol,bool isALoop)
{
	clip = Mix_LoadWAV(path.c_str());

	if (clip == nullptr)
	{
		EDebug::LogError("cannot find clip");
	}
	SetVolume(vol);
	SetLoop(isALoop);
	if (playOnAwake)
	{
		Play();
	}
}

EAudioChunkComponent::~EAudioChunkComponent()
{
	Stop();
	Clear();
}

void EAudioChunkComponent::Play()
{
	int loopcount = 0;
	int playChannel = -1;
	if (IsLooped())
	{
		loopcount = -1;
	}
	clipChannel = Mix_PlayChannel(playChannel,clip, loopcount);
	
}

void EAudioChunkComponent::Stop()
{
	//pause = false;
	//Mix_HaltChannel(clipChannel);
}

void EAudioChunkComponent::Pause(bool status)
{
	if (status)
		Mix_Pause(clipChannel);
	else
		Mix_Resume(clipChannel);
	pause = status;
}

void EAudioChunkComponent::SetLoop(bool status)
{
	loop = status;
}

bool EAudioChunkComponent::IsPlaying()
{
	int status =  Mix_Playing(clipChannel);	
	return status==1;
}

bool EAudioChunkComponent::IsPaused()
{
	return pause;
}

bool EAudioChunkComponent::IsLooped()
{
	return loop;
}

void EAudioChunkComponent::SetVolume(int range)
{
	volume = range;
	Mix_VolumeChunk(clip,volume);
}

int EAudioChunkComponent::GetVolume()
{
	return volume;
}

void EAudioChunkComponent::Clear()
{
	if (clip != nullptr)
	{
		Mix_FreeChunk(clip);
	}
}

