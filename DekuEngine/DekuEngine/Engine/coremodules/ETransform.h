#pragma once
#ifndef ETRANSFORm_H
#define ETRANSFORm_H
#include "EVector.h"
struct ETransform
{
public:
	ETransform();
	EVector3F position;
	float rotationAngle;
	EVector3F scale = EVector3F(1,1,1);
	void Translate(EVector3F val);

};
#endif // !ETRANSFOR_H


