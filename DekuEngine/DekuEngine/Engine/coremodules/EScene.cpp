#include "EScene.h"

EScene::EScene(string name)
{
	sceneName = name;
}

EScene::~EScene()
{
	Clear();
}

void EScene::Initialize()
{
	EGameObject::Initialize();
	camera = new ECamera(EWindow::SCREEN_WIDTH, EWindow::SCREEN_HEIGHT);
	uiController = new EUIController();
	if (uiController != nullptr)
	{
		uiController->Initialize();
	}
}

void EScene::InputHandler()
{
	EGameObject::InputHandler();
	if (!sceneActive)
		return;
	for (int i = 0; i < sceneInputObjects.size(); i++)
	{
		sceneInputObjects[i]->InputHandler();
	}
	if (uiController != nullptr)
	{
		uiController->InputHandler();
	}
}

void EScene::Update(float dt)
{
	EGameObject::Update(dt);
	if (!sceneActive)
		return;
	//update world
	for (int i = 0; i < sceneGameObjects.size(); i++)
	{
		sceneGameObjects[i]->Update(dt);
	}
	//update ui
	if (uiController != nullptr)
	{
		uiController->Update(dt);
	}
}

void EScene::Render()
{
	if (!sceneActive)
		return;
	//render world
	std::sort(sceneGameObjects.begin(), sceneGameObjects.end(), [](const EGameObject* lhs,const EGameObject* rhs) { return lhs->renderOrder < rhs->renderOrder; });
	for (int i = 0; i < sceneGameObjects.size(); i++)
	{
		sceneGameObjects[i]->Render();
	}
	//render ui 
	if (uiController != nullptr)
	{
		uiController->Render();
	}
}

bool EScene::SortRenderOrder(EGameObject& a, EGameObject& b)
{
	if (a.renderOrder < b.renderOrder)
		return true;
	return false;
}

void EScene::SetSceneName(string val)
{
	sceneName = val;
}

string EScene::GetSceneName()
{
	return sceneName;
}

void EScene::AddGameObjectToScene(EGameObject* gam)
{
	if (std::find(sceneGameObjects.begin(), sceneGameObjects.end(), gam) != sceneGameObjects.end())
	{
		EDebug::LogWarning("List already contains this object");
	}
	else 
	{
		sceneGameObjects.push_back(gam);
	}

}

void EScene::RemoveGameObjectFromScene(EGameObject* gam)
{
	if (std::find(sceneGameObjects.begin(), sceneGameObjects.end(), gam) != sceneGameObjects.end())
	{
		sceneGameObjects.erase(std::remove(sceneGameObjects.begin(), sceneGameObjects.end(), gam), sceneGameObjects.end());
	}
	else
	{
		EDebug::LogError("List does not contains the object");
	}

}

void EScene::AddInputObjectsToScene(EGameObject* gam)
{
	if (std::find(sceneInputObjects.begin(), sceneInputObjects.end(), gam) != sceneInputObjects.end())
	{
		EDebug::LogWarning("List already contains this object");
	}
	else
	{
		sceneInputObjects.push_back(gam);
	}
}

void EScene::RemoveInputObjectsFromScene(EGameObject* gam)
{
	if (std::find(sceneInputObjects.begin(), sceneInputObjects.end(), gam) != sceneInputObjects.end())
	{
		sceneInputObjects.erase(std::remove(sceneInputObjects.begin(), sceneInputObjects.end(), gam), sceneInputObjects.end());
	}
	else
	{
		EDebug::LogError("List does not contains the object");
	}
}

void EScene::Clear()
{
	sceneActive = false;
	sceneUnload = false;
	for (int i = 0; i < sceneInputObjects.size(); i++)
	{
		if (sceneInputObjects[i] != nullptr)
		{
			if (std::find(sceneGameObjects.begin(), sceneGameObjects.end(), sceneInputObjects[i]) != sceneGameObjects.end())
			{

			}
			else
			{
				delete sceneInputObjects[i];
			}
		}
		
	}
	for (int i = 0; i < sceneGameObjects.size(); i++)
	{
		if(sceneGameObjects[i] != nullptr)
			delete sceneGameObjects[i];
	}
	sceneGameObjects.clear();
	sceneInputObjects.clear();
	if (camera != nullptr)
		delete camera;
	camera = nullptr;
	if (uiController != nullptr)
		delete uiController;
	uiController = nullptr;
}

void EScene::UnloadScene()
{
	sceneUnload = true;
}
