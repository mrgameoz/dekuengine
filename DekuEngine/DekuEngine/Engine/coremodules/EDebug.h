#pragma once
#ifndef EDEBUG_H
#define EDEBUG_H
#include <stdio.h>
#include <string>
#include <SDL.h>
using namespace std;


namespace EDebug
{
	//updates log with given text
	void Log(string logtext);
	//updates log as warning
	void LogWarning(string logtext);
	//updates log as error
	void LogError(string logtext);
}
#endif // !EDEBUG_H
