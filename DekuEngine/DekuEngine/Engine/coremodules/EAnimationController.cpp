#include "EAnimationController.h"

EAnimationController::EAnimationController(bool playOnAwake)
{
	if (playOnAwake)
	{
		Play();
	}
}

EAnimationController::~EAnimationController()
{
	Clear();
}

void EAnimationController::UpdateClip(float deltaTime)
{
	if (currentClip == nullptr)
	{
		EDebug::LogError("current clip is Unassigned");
		return;
	}
	if (!CanPlay())
		return;
	currentClip->UpdateClip();
	AnimatorFlow();
}


void EAnimationController::AnimatorFlow()
{

}

EAnimationClip* EAnimationController::GetCurrentClip()
{
	return currentClip;
}

EAnimationClip* EAnimationController::SwitchToClip(EAnimationClip* clip)
{
	clip->ResetClipToStart();
	currentClip = clip;
	currentClip->Play();
	return currentClip;
}

void EAnimationController::Play()
{
	play = true;
}

void EAnimationController::Pause(bool pStatus)
{
	pause = pStatus;
}

bool EAnimationController::CanPlay()
{
	if (play && !pause)
		return true;
	return false;
}

bool EAnimationController::IsPlaying()
{
	return play;
}

bool EAnimationController::IsPaused()
{
	return pause;
}

void EAnimationController::Clear()
{
	currentClip = nullptr;
	gameObject = nullptr;
}
