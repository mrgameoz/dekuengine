#ifndef EANIMATIONCLIP_H
#define EANIMATIONCLIP_H
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "ETransform.h"
#include <vector>
using namespace std;
class EAnimationClip
{
public:
	//create a animation clip with name,totalframe,position x,y ,width,height,loop
	EAnimationClip(string name,int t,int x,int y,int w,int h,bool loop=false);
	~EAnimationClip();
	//total frames of the animation clip
	int totalFrames;
	//current frame of the animation
	int currentFrame=0;
	//should the animation loop?
	bool loop;
	//speed of the animation
	float speed;
	//number of rects in the sheet
	vector<SDL_Rect> sheet;
	//current rect in the sheet;
	SDL_Rect* currentRect;
	//run the animation
	SDL_Rect* GetCurrentRect();
	//Update the clip
	void UpdateClip();
	//play animation in reverse
	void SetClipReverse();
	//play animation in forward order , this is default order you can use it reset animation frames if you have reversed
	void ResetClipReverse();
	//has animation ended
	bool ClipEnded();
	//gets name of the clip
	bool CompareName(string pName);
	//resets clip to first frame 
	void ResetClipToStart();
	//plays clip
	void Play();
	//pause clip
	void Pause(bool status);
	//stop clip
	void Stop();
	//checks if can play clip
	bool CanPlay();
	//checks if clip is playing
	bool IsPlaying();
	//checks if clip is paused
	bool IsPaused();
	//checks if clip is stopped
	bool IsStopped();
private:
	//has the clip ended
	bool clipEnd = false;
	//play the clip reverse
	bool clipReverse;
	//name of the clip
	string name;
	//
	bool play = false;
	bool pause = false;
	bool stop = false;
	//clear
	void Clear();
};
#endif

