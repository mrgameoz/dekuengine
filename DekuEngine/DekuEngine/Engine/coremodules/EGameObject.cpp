#include "EGameObject.h"

EGameObject::EGameObject()
{
	stopUpdate = false;
	stopRender = false;
	stopInput = false;
	name = "GameObject";
	tag = "Default";
}

EGameObject::~EGameObject()
{
	Clear();
}

void EGameObject::Clear()
{
	
}

void EGameObject::Initialize()
{
	initialized = true;
}

void EGameObject::InputHandler()
{
	if (stopInput)
		return;

}

void EGameObject::Update(float dt)
{
	if (stopUpdate)
		return;
	if (!initialized)
	{
		Initialize();
	}

}

void EGameObject::Render()
{
	if (stopRender)
		return;

}

bool EGameObject::CompareTag(string val)
{
	return tag == val;
}

void EGameObject::StopRender(bool val)
{
	stopRender = val;
}

bool EGameObject::IsRenderStopped()
{
	return stopRender;
}

void EGameObject::StopInput(bool val)
{
	stopInput = val;
}

bool EGameObject::IsInputHandlerStopped()
{
	return stopInput;
}

void EGameObject::StopUpdate(bool val)
{
	stopUpdate = val;
}

bool EGameObject::IsUpdateStopped()
{
	return stopUpdate;
}

