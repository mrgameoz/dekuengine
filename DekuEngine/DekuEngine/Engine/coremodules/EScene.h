#ifndef ESCENE_H
#define ESCENE_H

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include "EGameObject.h"
#include <vector>
#include "UI\EUIController.h"
#include "ECamera.h"
#include <algorithm>
using namespace std;
class EScene:public EGameObject
{

public:
	//create scene
	EScene(string sceneName);
	virtual ~EScene();
	//scene number as per appilcation scene list
	int sceneNumber;
	//scene active status
	bool sceneActive = false;
	//is scene ready for unload
	bool sceneUnload = false;
	//camera is created when scene is intialized with screen width and screen height
	ECamera* camera = nullptr;
	//UI controller for the scene
	EUIController* uiController = nullptr;
private:
	std::string sceneName;
	vector<EGameObject*> sceneGameObjects;
	vector<EGameObject*> sceneInputObjects;
public:
	//get scene name
	std::string GetSceneName();
	//Sets Scene Name
	void SetSceneName(string val);
	//switch scene
	void UnloadScene();
	//add gameobjects to scene so, it updates and renders
	void AddGameObjectToScene(EGameObject* gameObject);
	//remove gameobject from scene 
	void RemoveGameObjectFromScene(EGameObject* gameObject);
	//add gameobjects to scene so, it takes their inputs
	void AddInputObjectsToScene(EGameObject* gameObject);
	//remove InputObjects from scene 
	void RemoveInputObjectsFromScene(EGameObject* gameObject);
	//intializes the scene
	virtual void Initialize() override;
	//handles input
	virtual void InputHandler() override;
	//handles update
	virtual void Update(float dt) override;
	//handles rendering
	virtual void Render() override;
protected:
	//clear scene
	virtual void Clear();
private:
	bool SortRenderOrder(EGameObject& a, EGameObject& b);


};
#endif // !ESCENE_H
