#pragma once
#ifndef EFONTTEXTURE_H
#define EFONTTEXTURE_H
#include "ETexture.h"
#include <SDL_ttf.h>

using namespace std;
class EText : public ETexture
{
public:
	//Initializes variables
	EText(ETransform* trans, std::string fontpath, SDL_Color textColor = { 0, 0, 0 });
	~EText();
	//Load Font
	bool LoadFont(std::string fontpath);
	//Sets text
	bool SetText(std::string text);
	//Get text
	string GetText();
	//set color
	void SetColor(SDL_Color textColor);
	//Get color
	SDL_Color GetColor();
	//set font size
	void SetFontSize(int size);
	//Get font size
	int GetFontSize();
private:
	TTF_Font* font = nullptr;
	int fontSize;
	string text;
	string fontpath;
	SDL_Color textColor = { 0, 0, 0 };
	//Deallocates memory
	void Clear();
	//reload font 
	void ReloadFont();
};

#endif // !EFONTTEXTURE_H

