#include "ETexture.h"

void ETexture::Render(SDL_Rect* clip,double angle,EVector3I center,SDL_RendererFlip flip)
{
	int x = GetWorldDrawPositionX();
	int y = GetWorldDrawPositionY();
	//Set rendering space and render to world
	textureRect = { x, y, GetScaledWidth(), GetScaledHeight() };
	SDL_Point pointcenter;
	pointcenter.x = center.x;
	pointcenter.y = center.y;
	//Set clip rendering dimensions
	if (clip != NULL)
	{
		textureRect.w = clip->w*transform->scale.x;
		textureRect.h = clip->h*transform->scale.y;
	}

	//Render to position
	SDL_RenderCopyEx(gRenderer, texture, clip, &textureRect, angle, &pointcenter, flip);

	//SDL_RenderCopy(gRenderer, sprite, clip, &renderQuad);
}

void ETexture::RenderOnScreen(SDL_Rect* clip, double angle, EVector3I center,SDL_RendererFlip flip)
{
	int x = GetScreenDrawPositionX();
	int y = GetScreenDrawPositionY();
	//Set rendering space and render to screen
	textureRect = { x, y, (int)(GetScaledWidth()*EWindow::xScreenScaleDiff), (int)(GetScaledHeight()*EWindow::yScreenScaleDiff) };
	SDL_Point pointcenter;
	pointcenter.x = center.x;
	pointcenter.y = center.y;
	//Set clip rendering dimensions
	if (clip != NULL)
	{
		textureRect.w = clip->w*transform->scale.x;
		textureRect.h = clip->h*transform->scale.y;
	}

	//Render to screen
	SDL_RenderCopyEx(gRenderer, texture, clip, &textureRect, angle, &pointcenter, flip);

	//SDL_RenderCopy(gRenderer, sprite, clip, &renderQuad);
}

ETexture::ETexture(ETransform* trans, std::string path)
{
	//Initialize
	texture = NULL;
	width = 0;
	height = 0;
	transform = trans;
	gRenderer = EWindow::GetRenderer();
	LoadFromFile(path);
}


ETexture::~ETexture()
{
	//Deallocate
	Clear();
	gRenderer = nullptr;
	transform = nullptr;
}

void ETexture::SetPivotPoint(EVector3F point)
{
	pivotPoint = point;
}

EVector3F ETexture::GetPivotPoint()
{
	return pivotPoint;
}

bool ETexture::LoadFromFile(std::string path)
{

	
	////Get rid of preexisting texture
	Clear();
	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else
	{
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
		else
		{
			//Get image dimensions
			width = loadedSurface->w;
			height = loadedSurface->h;
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	texture = newTexture;
	return texture != NULL;
}

void ETexture::Clear()
{
	//Free texture if it exists
	if (texture != NULL)
	{
		SDL_DestroyTexture(texture);
		texture = NULL;
		width = 0;
		height = 0;
	}
}


void ETexture::SetColor(Uint8 red, Uint8 green, Uint8 blue)
{
	//Modulate texture rgb
	SDL_SetTextureColorMod(texture, red, green, blue);
}

void ETexture::SetBlendMode(SDL_BlendMode blending)
{
	//Set blending function
	SDL_SetTextureBlendMode(texture, blending);
}

void ETexture::SetAlpha(Uint8 alpha)
{
	//Modulate texture alpha
	SDL_SetTextureAlphaMod(texture, alpha);
}

int ETexture::GetWidth()
{
	return width;
}

int ETexture::GetHeight()
{
	return height;
}

int ETexture::GetScaledWidth()
{
	return width*transform->scale.x;
}

int ETexture::GetScaledHeight()
{
	return height*transform->scale.y;
}

SDL_Rect* ETexture::GetTextureRect()
{
	return &textureRect;
}


EVector3F ETexture::GetMidPoint()
{
	EVector3F midpoint;
	float x = transform->position.x + (GetScaledWidth()/2);
	float y = transform->position.y + (GetScaledHeight()/2);
	midpoint.x = x;
	midpoint.y = y;
	return midpoint;
}

EVector3F ETexture::GetMidPointOnScreen()
{
	EVector3F midpoint;
	float screenX = transform->position.x;
	float width = (GetScaledWidth()*EWindow::xScreenScaleDiff)/2;
	float x = screenX + (width/EWindow::SCREEN_WIDTH);
	float screenY = transform->position.y;
	float height = (GetScaledHeight()*EWindow::yScreenScaleDiff)/2;
	float y = screenY + (height/EWindow::SCREEN_HEIGHT);
	midpoint.x = x;
	midpoint.y = y;
	return midpoint;
}


int ETexture::GetWorldDrawPositionX()
{
	int pos;
	//pos = (int)transform->position.x -ECamera::GetCameraPosition().x;
	int x = (int)transform->position.x - ECamera::GetCameraPosition().x;
	int pivotmod = pivotPoint.x*GetScaledWidth();
	pos = x - pivotmod;
	return pos;
}

int ETexture::GetWorldDrawPositionY()
{
	int pos;
	//pos = (int)transform->position.y - ECamera::GetCameraPosition().y;
	int y = (int)transform->position.y - ECamera::GetCameraPosition().y;
	int pivotmod = pivotPoint.y*GetScaledHeight();
	pos = y - pivotmod;
	return pos;
}

int ETexture::GetScreenDrawPositionX()
{
	int pos;
	int screenX = EWindow::SCREEN_WIDTH * transform->position.x;
	int pivotmod = (pivotPoint.x*(GetScaledWidth()*EWindow::xScreenScaleDiff));
	pos = screenX - pivotmod;
	return pos;
}

int ETexture::GetScreenDrawPositionY()
{
	int pos;
	int screenY = EWindow::SCREEN_HEIGHT * transform->position.y;
	int pivotmod = (pivotPoint.y*(GetScaledHeight()*EWindow::yScreenScaleDiff));
	pos = screenY - pivotmod;
	return pos;
}
