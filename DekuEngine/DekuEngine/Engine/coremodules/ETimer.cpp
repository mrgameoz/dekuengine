#include "ETimer.h"

ETimer::ETimer()
{
	startTicks = 0;
	pausedTicks = 0;
	started = false;
	paused = false;
}

void ETimer::Start()
{
	started = true;
	paused = false;
	startTicks = SDL_GetTicks();
	pausedTicks = 0;
}

void ETimer::Stop()
{
	startTicks = 0;
	pausedTicks = 0;
	started = false;
	paused = false;
}

void ETimer::Pause(bool status)
{
	//if status is pause
	if (status)
	{
		if (started && !paused)
		{
			//Pause the timer
			paused = true;

			//Calculate the paused ticks
			pausedTicks = SDL_GetTicks() - startTicks;
			startTicks = 0;
		}
	}	
	else
	{
		if (started && paused)
		{
			//Unpause the timer
			paused = false;

			//Reset the starting ticks
			startTicks = SDL_GetTicks() - pausedTicks;

			//Reset the paused ticks
			pausedTicks = 0;
		}
	}
}

Uint32 ETimer::GetTicks()
{
	//The actual timer time
	Uint32 time = 0;

	//If the timer is running
	if (started)
	{
		//If the timer is paused
		if (paused)
		{
			//Return the number of ticks when the timer was paused
			time = pausedTicks;
		}
		else
		{
			//Return the current time minus the start time
			time = SDL_GetTicks() - startTicks;
		}
	}
	return time;
}

float ETimer::GetDeltaTime()
{
	float time = 0;

	if (started && !paused)
	{
		currentTime = SDL_GetTicks();
		if (currentTime > lastTime)
		{
			time = ((float)currentTime - lastTime) / 1000;
			lastTime = currentTime;
		}
	}
	return time;
}

Uint32 ETimer::GetTimeSinceStart()
{
	return SDL_GetTicks();
}

bool ETimer::isStarted()
{
	return started;
}

bool ETimer::isPaused()
{
	return paused && started;
}
