#ifndef EAUDIOMUSICCOMPONENT_H
#define EAUDIOMUSICCOMPONENT_H
#pragma once
#include <SDL_mixer.h>
#include "EGameObject.h"
#include "EDebug.h"
class EAudioMusicComponent
{
public:
	//intialize your audio source
	EAudioMusicComponent(string path,bool playOnAwake,int volume=10, bool isALoop=true, float fadeTime=0);
	~EAudioMusicComponent();
	//plays clip in the audio source
	void Play();
	//stop clip in the audio source
	void Stop();
	//pauses clip in the audio source
	void Pause(bool status);
	//loop the audio
	void SetLoop(bool status);
	//checks if audio source is playing
	bool IsPlaying();
	//checks if audio source is paused
	bool IsPaused();
	//checks if audio source is looped
	bool IsLooped();
	//set volume 0-128
	void SetVolume(int range);
	//get volume
	int GetVolume();
	Mix_Music* clip = nullptr;

private:
	bool loop = false;
	bool pause = false;
	float volume = 0;
	float fadeTime = 0;
	//clear
	void Clear();
};
#endif // ! EAUDIOSOURCE_H


