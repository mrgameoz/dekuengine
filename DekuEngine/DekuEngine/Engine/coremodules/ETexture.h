#pragma once
#ifndef ETEXTURE_H
#define ETEXTURE_H
#include "EWindow.h"
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "ETransform.h"
#include "EDebug.h"
#include "ECamera.h"
#include <vector>

using namespace std;
class ETexture
{
public:
	//Initializes variables
	ETexture(ETransform* trans, std::string path);
	//Deallocates memory
	virtual ~ETexture();

	//Loads image at specified path
	bool LoadFromFile(std::string path);

	//Set color modulation
	void SetColor(Uint8 red, Uint8 green, Uint8 blue);

	//Set blending
	void SetBlendMode(SDL_BlendMode blending);

	//Set alpha modulation
	void SetAlpha(Uint8 alpha);

	//Renders texture at given world point(used for objects in game)
	void Render(SDL_Rect* clip = NULL, double angle = 0.0, EVector3I center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Renders texture at given screen point(used for ui objects)
	void RenderOnScreen(SDL_Rect* clip = NULL, double angle = 0.0, EVector3I center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//SetPivotPoint(0.0f - 1.0f);
	void SetPivotPoint(EVector3F point);

	//returns pivot point
	EVector3F GetPivotPoint();

	//returns midpoint of texture on world space
	EVector3F GetMidPoint();

	//returns midpoint of texture on screen space
	EVector3F GetMidPointOnScreen();

	//Gets image dimensions
	int GetWidth();
	int GetHeight();
	int GetScaledWidth();
	int GetScaledHeight();
	SDL_Rect* GetTextureRect();
	SDL_Renderer* gRenderer = nullptr;

protected:
	//clears sprites base variables
	virtual void Clear();
	//Image width
	int width;
	//Image height
	int height;
	//rect of the texture
	SDL_Rect textureRect;
	//The actual texture
	SDL_Texture* texture = NULL;
private:

	ETransform* transform;
	EVector3F pivotPoint;

	//Returns world draw position of texture in x axis
	int GetWorldDrawPositionX();
	//Returns world draw position of texture in y axis
	int GetWorldDrawPositionY();
	//Returns screen draw position of texture in x axis
	int GetScreenDrawPositionX();
	//Returns screen draw position of texture in y axis
	int GetScreenDrawPositionY();

};

#endif // !ETEXTURE_H

