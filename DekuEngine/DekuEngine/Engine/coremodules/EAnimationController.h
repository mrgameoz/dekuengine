#ifndef EANIMATIONCONTROLLER_H
#define EANIMATIONCONTROLLER_H
#pragma once
#include "EAnimationClip.h"
#include "EGameObject.h"
#include "EDebug.h"

class EAnimationController
{
public:
	//gameObject to which animator is added
	EGameObject* gameObject;
	//intialize your animator 
	EAnimationController(bool playOnAwake);
	virtual ~EAnimationController();
	//used to control the animation transition and center for state machine
	virtual void UpdateClip(float deltaTime);
	//sets state machine for animator
	virtual void AnimatorFlow();
	//get current Clip
	EAnimationClip* GetCurrentClip();
	//plays animator
	void Play();
	//pause animator
	void Pause(bool status);
	//checks if animator can play
	bool CanPlay();
	//checks if animator is playing
	bool IsPlaying();
	//checks if animator is paused
	bool IsPaused();
protected:
	EAnimationClip* currentClip;
	//Switchs to the clip
	EAnimationClip* SwitchToClip(EAnimationClip* clip);
private:
	bool play = false;
	bool pause = false;
	//clear
	void Clear();
};

#endif // !EANIMATIONCLIP_H


