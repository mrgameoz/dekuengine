#include "ECallbackAction.h"

ECallbackID ECallbackAction::CreateCallbackAction(Uint32 delay, SDL_TimerCallback callbackFuntion, void* param)
{
	ECallbackID callbackID = SDL_AddTimer(delay, callbackFuntion, param);
	return callbackID;
}

bool ECallbackAction::RemoveCallbackAction(ECallbackID id)
{
	return SDL_RemoveTimer(id);
}
