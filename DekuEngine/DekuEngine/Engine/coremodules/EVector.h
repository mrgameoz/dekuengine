#pragma once
#ifndef EVECTOR_H
#define EVECTOR_H
struct EVector3F
{
public:
	EVector3F(float x=0,float y=0,float z=0);
	float x;
	float y;
	float z;
	void Add(EVector3F vector);
};

struct EVector3I
{
public:
	EVector3I(int x = 0, int y = 0, int z = 0);
	int x;
	int y;
	int z;
};

#endif // !EVECTOR_H


