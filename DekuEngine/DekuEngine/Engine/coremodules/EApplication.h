#pragma once
#ifndef EAPPLICATION_H
#define EAPPLICATION_H
#include "EWindow.h"
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <string>
#include "EInput.h"
#include "EScene.h"
#include "ETimer.h"
#include "EUtil.h"
#include <iostream>


using namespace std;
class EApplication {

private:
	//The window we'll be rendering to
	EWindow* gWindow = nullptr;
	//The window renderer
	SDL_Renderer* gRenderer;
	//timer
	ETimer timer_App;
	//current scene no
	int currentSceneNo = 0;
	//sets or gets active scene
	EScene* activeScene = nullptr;
	//total counted frames 
	int countedFrames = 0;
	//is application running
	static bool applicationRunning;

private:
	//handles input
	void InputHandler();
	//handles update function
	void UpdateHandler();
	//handles rendering
	void RenderHandler();


protected:
	bool SetActiveSceneTo(EScene* scene);
	//set a scene to active scene
	virtual void LoadScene();
	//play the active scene 
	virtual void PlayScene();
	//unload the active scene
	virtual bool UnloadScene();

public:
	//initialize application with screen width and height
	//if design width and height are not specified screen w/h is taken as design h/w
	//design w/h is must for screen ui to scale accordingly
	virtual bool InitApplication(int screenWidth,int screenHeight, int designwidth = 0, int designheight = 0);
	//return ApplicationRunning
	static bool IsApplicationRunning();
	//quits application
	static void QuitApplication();
	//closes application after deleting - must not be called directly (use quit application instead)
	virtual void Clear();
	//main run function
	virtual void Run();
	//close current Scene.
	void CloseCurrentScene();



};

#endif // !EAPPLICATION_H
