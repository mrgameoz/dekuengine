#pragma once
#ifndef EGAMEOBJECT_H
#define EGAMEOBJECT_H
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "ETransform.h"
#include "EDebug.h"

using namespace std;
class EGameObject {
public:
	EGameObject();
	virtual ~EGameObject();
	string name, tag;
	ETransform transform;
	int renderOrder=0;
protected:
	bool stopRender = false;
	bool stopUpdate = false;
	bool stopInput = false;

private:
	bool initialized = false;

public:
	//initialize at first tick
	virtual void Initialize();
	//handle inputs
	virtual void InputHandler();
	//update objects 
	virtual void Update(float dt);
	//set rendering elements
	virtual void Render();
	//returns if tags are same
	bool CompareTag(string val);
	//stop rendering
	void StopInput(bool val);
	//stop rendering
	void StopRender(bool val);
	//stops update
	void StopUpdate(bool val);
	//checks if input handler is stopped
	bool IsInputHandlerStopped();
	//checks if render is stopped
	bool IsRenderStopped();
	//checks if update is stopped
	bool IsUpdateStopped();

protected:
	void Clear();
};

#endif // !EGAMEOBJECT_H



