#include "EDebug.h"

void EDebug::Log(string text)
{
	string _log = "[Log]: ";
	string newLine = "\n";
	string log = _log + text + newLine;
	SDL_Log(log.c_str());
}

void EDebug::LogWarning (string text)
{
	string _warning = "[Warning]: ";
	string newLine = "\n";
	string log = _warning + text + newLine;
	SDL_Log(log.c_str());
}

void EDebug::LogError(string text)
{
	string _error = "[Error]: ";
	string newLine = "\n";
	string log = _error + text + newLine;
	SDL_Log(log.c_str());
}