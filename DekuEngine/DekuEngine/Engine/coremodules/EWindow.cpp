#include "EWindow.h"

int EWindow::SCREEN_HEIGHT = 800;
int EWindow::SCREEN_WIDTH = 600;
int EWindow::DESIGN_WIDTH = 0;
int EWindow::DESIGN_HEIGHT = 0;
float EWindow::xScreenScaleDiff = 0.0f;
float EWindow::yScreenScaleDiff = 0.0f;
SDL_Renderer* EWindow::gRenderer = nullptr;

EWindow::EWindow(int width, int height,int dwidth,int dheigth)
{
	SCREEN_WIDTH = width;
	SCREEN_HEIGHT = height;
	if (dwidth > 0 && dheigth > 0)
	{
		SetDesignSize(dwidth, dheigth);
	}
	else
	{
		SetDesignSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	}
	
}

EWindow::~EWindow()
{
	Free();
}


bool EWindow::InitWindow()
{
	//Initialization flag
	bool success = true;
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		{
			printf("Warning: Linear texture filtering not enabled!");
		}
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
		SDL_DisplayMode current;
		SDL_GetCurrentDisplayMode(0, &current);
		//Create window
		gWindow = SDL_CreateWindow("Engine", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{

			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
		}
	}
	
	RecorrectWindowSize();

	return success;
}

void EWindow::SetDesignSize(int w, int h)
{
	DESIGN_WIDTH = w;
	DESIGN_HEIGHT = h;
	SetScaleSizes();

}

void EWindow::SetScaleSizes()
{
	xScreenScaleDiff = (float)SCREEN_WIDTH / (float)DESIGN_WIDTH;
	yScreenScaleDiff = (float)SCREEN_HEIGHT / (float)DESIGN_HEIGHT;
}

void EWindow::SetScreenSize(int w, int h)
{
	SCREEN_WIDTH = w;
	SCREEN_HEIGHT = h;
}

SDL_Renderer* EWindow::GetRenderer()
{
	return gRenderer;
}

void EWindow::RecorrectWindowSize()
{
	if (EUtil::GetCurrentPlatform() != EUtil::Platform::Android || EUtil::GetCurrentPlatform() != EUtil::Platform::iOS)
	{
		int w = 0;
		int h = 0;
		SDL_GetWindowSize(gWindow, &w, &h);
		if (SCREEN_WIDTH != w)
		{
			SCREEN_WIDTH = w;
		}
		if (SCREEN_HEIGHT != h)
		{
			SCREEN_HEIGHT = h;
		}
	}
	SetScaleSizes();
}

void EWindow::Free()
{

	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;
	SDL_Quit();
}



