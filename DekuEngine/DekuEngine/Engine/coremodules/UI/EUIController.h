#ifndef EUICONTROLLER_H
#define EUICONTROLLER_H

#include "EUIComponents.h"
#include "../EGameObject.h"
#include <vector>
#include <algorithm>


using namespace std;
//UI Controller
class EUIController:public EGameObject
{
public:
	bool isInitialized = false;

	//create UI Controller
	EUIController();
	virtual ~EUIController();
	//intializes the UI
	virtual void Initialize() override;
	//handles input
	virtual void InputHandler() override;
	//handles update
	virtual void Update(float dt) override;
	//handles rendering
	virtual void Render() override;
	//add ui component to controller
	void AddUIComponents(EGameObject* component);
	//remove  ui component from controller
	void RemoveUIComponents(EGameObject* component);

protected:
	//clear
	void Clear();

private:
	vector<EGameObject*> controllerUIObjects;

};
#endif // !ESCENE_H
