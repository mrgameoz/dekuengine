#include "EUIComponents.h"

//UI Image
EUIImage::EUIImage(string path ,EVector3F pos, EVector3F scale)
{
	transform.position = pos;
	transform.scale = scale;
	texture = new ETexture(&transform, path);
}

EUIImage::~EUIImage()
{
	Clear();
}

void EUIImage::Update(float dt)
{
	EGameObject::Update(dt);

}

void EUIImage::Render()
{
	EGameObject::Render();
	if (texture != nullptr)
	{
		texture->RenderOnScreen(NULL,transform.rotationAngle);
	}
}

ETexture* EUIImage::GetTexture()
{
	return texture;
}

void EUIImage::Clear()
{
	delete texture;
	texture = nullptr;
}

//UIText
EUIText::EUIText(string path, EVector3F pos, EVector3F scale)
{
	transform.position = pos;
	transform.scale = scale;
	textComponent = new EText(&transform, path);
}

EUIText::~EUIText()
{
	Clear();
}

void EUIText::Update(float dt)
{
	EGameObject::Update(dt);

}

void EUIText::Render()
{
	EGameObject::Render();
	if (textComponent != nullptr)
	{
		textComponent->RenderOnScreen(NULL, transform.rotationAngle);
	}
}

EText* EUIText::GetTextComponent()
{
	return textComponent;
}

void EUIText::Clear()
{
	delete textComponent;
	textComponent = nullptr;
}

//UIButton
EUIButton::EUIButton(EVector3F pos, EVector3F scale)
{
	transform.position = pos;
	transform.scale = scale;
	SetImageForState("assets/b0.png",Status::Base);
	text = new EUIText();
	text->transform.position = transform.position;
}

EUIButton::~EUIButton()
{
	Clear();
}


void EUIButton::InputHandler()
{
	EGameObject::InputHandler();
	SetButtonStatus();

}

void EUIButton::Update(float dt)
{
	EGameObject::Update(dt);

}

void EUIButton::Render()
{
	EGameObject::Render();
	if(images[buttonStatus]!=nullptr)
		images[buttonStatus]->Render();
	if (text != nullptr)
		text->Render();

}

void EUIButton::SetButtonStatus()
{
	if (!IsPointerInRect())
	{
		buttonStatus = Status::Base;
	}
	else if (IsPointerInRect() && !IsDoingAction())
	{
		buttonStatus = Status::MouseHover;
	}
	else if (IsPointerInRect() && (EInput::GetMouseDown(KeyCode::KEY_LMB) || EInput::GetTouchDown()))
	{
		buttonStatus = Status::MouseDown;
	}
	else if (IsPointerInRect() && (EInput::GetMouseUp(KeyCode::KEY_LMB) || EInput::GetTouchUp()))
	{
		buttonStatus = Status::MouseUp;
	}
}

bool EUIButton::IsPointerInRect()
{
	SDL_Rect* rect = images[buttonStatus]->GetTexture()->GetTextureRect();
	EVector3I pointerPos = EInput::GetMousePosition();
	bool status = true;
	if (pointerPos.x < rect->x)
	{
		status = false;
	}
	else if (pointerPos.x > rect->x + rect->w)
	{
		status = false;
	}
	else if (pointerPos.y < rect->y)
	{
		status = false;
	}
	else if (pointerPos.y > rect->y + rect->h)
	{
		status = false;
	}
	return status;
}

bool EUIButton::IsDoingAction()
{
	bool status = true;
	if (!EInput::GetMouseUp(KeyCode::KEY_LMB) && !EInput::GetMousePressed(KeyCode::KEY_LMB) && !EInput::GetMouseDown(KeyCode::KEY_LMB))
	{
		status = false;
	}
	return status;
}

bool EUIButton::OnHover()
{
	return buttonStatus == Status::MouseHover;
}

bool EUIButton::OnMouseDown()
{
	return buttonStatus == Status::MouseDown;
}

bool EUIButton::OnMouseUp()
{
	return buttonStatus == Status::MouseUp;
}

bool EUIButton::SetImageForState(string path, int no)
{
	images[no] = new EUIImage(path);
	images[no]->transform.position = transform.position;
	if (images[no] != nullptr)
	{
		return true;
	}
	return false;
}

void EUIButton::Clear()
{
	for (int i = 0; i < 4; i++)
	{
		if (images[i] != nullptr)
		{
			delete images[i];
			images[i] = nullptr;
		}
	}
	if (text != nullptr)
	{
		delete text;
		text = nullptr;
	}

}