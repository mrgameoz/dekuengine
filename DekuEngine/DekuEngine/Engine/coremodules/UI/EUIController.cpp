#include "EUIController.h"

EUIController::EUIController()
{

}

EUIController::~EUIController()
{
	Clear();
}


void EUIController::Initialize()
{
	EGameObject::Initialize();

}

void EUIController::InputHandler()
{
	EGameObject::InputHandler();
	for (int i = 0; i < controllerUIObjects.size(); i++)
	{
		controllerUIObjects[i]->InputHandler();
	}
}


void EUIController::Update(float dt)
{
	EGameObject::Update(dt);
	for (int i = 0; i < controllerUIObjects.size(); i++)
	{
		controllerUIObjects[i]->Update(dt);
	}
}

void EUIController::Render()
{
	EGameObject::Render();
	for (int i = 0; i < controllerUIObjects.size(); i++)
	{
		controllerUIObjects[i]->Render();
	}

}


void EUIController::AddUIComponents(EGameObject* component)
{
	if (std::find(controllerUIObjects.begin(), controllerUIObjects.end(), component) != controllerUIObjects.end())
	{
		EDebug::LogWarning("List already contains this object");
	}
	else
	{
		controllerUIObjects.push_back(component);
	}
}

void EUIController::RemoveUIComponents(EGameObject* component)
{
	if (std::find(controllerUIObjects.begin(), controllerUIObjects.end(), component) != controllerUIObjects.end())
	{
		controllerUIObjects.erase(std::remove(controllerUIObjects.begin(), controllerUIObjects.end(), component), controllerUIObjects.end());
	}
	else
	{
		EDebug::LogError("List does not contains the object");
	}
}


void EUIController::Clear()
{
	for (int i = 0; i < controllerUIObjects.size(); i++)
	{
		if (controllerUIObjects[i] != nullptr)
		{
			delete controllerUIObjects[i];
		}
	}
	controllerUIObjects.clear();
	isInitialized = false;
}



