#ifndef EUICOMPONENTS_H
#define EUICOMPONENTS_H

#include "../EGameObject.h"
#include "../EText.h"
#include "../EInput.h"

using namespace std;

//Create a UI Image
class EUIImage:public EGameObject
{
public:
	//Create a UI Image, position must be 0.0f-1.0f
	EUIImage(string path,EVector3F position= EVector3F(0.5f, 0.5f),EVector3F scale= EVector3F(1,1,1));
	~EUIImage();
	void Update(float dt) override;
	void Render() override;
	//Returns texture
	ETexture* GetTexture();
private:
	ETexture* texture = nullptr;
	void Clear();
};

class EUIText :public EGameObject
{
public:
	//Create a UI Image, position must be 0.0f-1.0f
	EUIText(string fontpath= "assets/arial.ttf", EVector3F position = EVector3F(0.5f, 0.5f), EVector3F scale = EVector3F(1, 1, 1));
	~EUIText();
	void Update(float dt) override;
	void Render() override;
	//Returns text texture
	EText* GetTextComponent();
private:
	EText* textComponent = nullptr;
	void Clear();
};

class EUIButton :public EGameObject
{
public:

	//Create a UI Image, position must be 0.0f-1.0f
	EUIButton(EVector3F position = EVector3F(0.5f, 0.5f), EVector3F scale = EVector3F(1, 1, 1));
	~EUIButton();
	void InputHandler() override;
	void Update(float dt) override;
	void Render() override;
	//is mouse down on the button
	bool OnMouseDown();
	//is mouse up on the button
	bool OnMouseUp();
	//is mouse over the button
	bool OnHover();
	//is mouse doing any action
	bool IsDoingAction();
	//is pointer in the button rect
	bool IsPointerInRect();
	//set image for state of button
	bool SetImageForState(string path, int stateNo);
	//button text
	EUIText* text;
	//button image array [0]Base,[1]MouseHover,[2]MouseDown,[3]MouseUp
	EUIImage* images[4];
	
private:
	enum Status
	{
		Base,
		MouseHover,
		MouseDown,
		MouseUp
	};
	Status buttonStatus = Status::Base;
	void SetButtonStatus();
	void Clear();
};
#endif // !EUICOMPONENTS_H
