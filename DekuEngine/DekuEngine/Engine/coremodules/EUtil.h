#pragma once
#ifndef EUTIL_H
#define EUTIL_H
#include <SDL.h>
#include <string>

using namespace std;
class EUtil {


public:
	static float fpsCount;
	//return fps
	static float GetFPS();
	//platform enums
	enum Platform { Unknown, Windows, Mac_OS_X, Linux, iOS, Android };
	//returns the current platform id
	//0-Unkown,1-Windows,2-Mac_OS_X,3-Linux,4-iOS,5-Android
	static int GetCurrentPlatform();
	//detect the platform
	static void DetectPlatform();
protected:
	static int currentPlatform;


};

#endif // !EUTIL_H
