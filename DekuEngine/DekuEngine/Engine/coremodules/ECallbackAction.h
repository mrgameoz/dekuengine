#pragma once
#ifndef ECALLBACKACTION_H
#define ECALLBACKACTION_H
#include <SDL.h>

typedef SDL_TimerID ECallbackID;

class ECallbackAction
{
public:
	//create a callback action (pass interval,function and parameter)
	static ECallbackID CreateCallbackAction(Uint32 delay, SDL_TimerCallback callbackFuntion, void* param);
	//remove the created callback action by passing the id
	static bool RemoveCallbackAction(ECallbackID CallbackID);

private:
	ECallbackAction();
};



#endif // !ETIMER_H



