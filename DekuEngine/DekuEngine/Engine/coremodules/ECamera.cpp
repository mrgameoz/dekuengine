#include "ECamera.h"

EVector3I ECamera::position = EVector3I(0,0);

ECamera::ECamera(int width, int height)
{
	viewport.x= 0;
	viewport.y = 0;
	SetCameraSize(width, height);	
	SDL_RenderSetViewport(EWindow::GetRenderer(), &viewport);
}

void ECamera::SetCameraPosition(EVector3I pos)
{
	if (pos.x < 0)
		pos.x = 0;
	if (pos.y < 0)
		pos.y = 0;
	position = pos;
	SetViewportPosition();
}

EVector3I ECamera::GetCameraPosition()
{
	return position;
}

void ECamera::SetCameraSize(int w,int h)
{
	width = w;
	height = h;
	SetViewportSize();
}

void ECamera::SetViewportPosition()
{
	viewport.x = position.x;
	viewport.y = position.y;
}

void ECamera::SetViewportSize()
{
	viewport.w = width;
	viewport.h = height;
}


