#pragma once
#ifndef ECAMERA_H
#define ECAMERA_H
#include <SDL.h>
#include <SDL_image.h>
#include "EVector.h"
#include "EWindow.h"
class ECamera
{
public:
	//Create camera with width and height
	ECamera(int width, int height);
	//Set camera in a position
	void SetCameraPosition(EVector3I position);
	//returns camera position
	static EVector3I GetCameraPosition();
	//Set camera width and height
	void SetCameraSize(int width, int height);

private:
	int width = 0;
	int height = 0;
	static EVector3I position;
	//viewport pos
	void SetViewportPosition();
	//viewport size
	void SetViewportSize();
	SDL_Rect viewport;
};
#endif // !ECAMERA_H


