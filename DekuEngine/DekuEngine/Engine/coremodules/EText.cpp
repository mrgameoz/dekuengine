#include "EText.h"


EText::EText(ETransform* trans, std::string path,SDL_Color color):ETexture(trans,path)
{
	//Initialize
	fontSize = 20;
	fontpath = path;
	textColor = color;
	if (LoadFont(fontpath))
	{
		SetText("New Text");
	}
}


EText::~EText()
{
	Clear();
}


bool EText::SetText(string pText)
{
	//Get rid of preexisting texture
	ETexture::Clear();
	text = pText;
	//The final texture
	SDL_Texture* newTexture = NULL;
	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(font, text.c_str(), textColor);
	if (textSurface == NULL)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}
	else
	{
		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(gRenderer, textSurface);
		if (newTexture == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			width = textSurface->w;
			height = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}

	//Return success
	texture = newTexture;
	return font != NULL;
}

string EText::GetText()
{
	return text;
}

bool EText::LoadFont(string path)
{
	bool success = true;

	//Open the font
	font = TTF_OpenFont(path.c_str(), fontSize);
	if (font == NULL)
	{
		printf("Failed to load font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}
	return success;
}

void EText::ReloadFont()
{
	if (LoadFont(fontpath))
	{
		SetText(text);
	}
}

void EText::SetColor(SDL_Color color)
{
	textColor = color;
	ReloadFont();
}

SDL_Color EText::GetColor()
{
	return textColor;
}

void EText::SetFontSize(int size)
{
	fontSize = size;
	ReloadFont();
}

int EText::GetFontSize()
{
	return fontSize;
}


void EText::Clear()
{
	if (font != nullptr)
	{
		TTF_CloseFont(font);
		font = nullptr;
	}

}