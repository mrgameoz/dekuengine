#include "EInput.h"


SDL_Event EInput::InputEvent = SDL_Event();
EVector3I EInput::mousePosition = EVector3I();
int EInput::pollcount = 0;
const Uint8*  EInput::currentKeyStates = nullptr;

void EInput::UpdateInput()
{
	UpdateMousePosition();
	pollcount = SDL_PollEvent(&InputEvent);
	currentKeyStates = SDL_GetKeyboardState(NULL);
}

void EInput::UpdateMousePosition()
{
	SDL_GetMouseState(&mousePosition.x, &mousePosition.y);
}

EVector3I EInput::GetMousePosition()
{
	return mousePosition;
}


bool EInput::GetMouseDown(int pressedkey)
{
	if (pollcount == 0)
		return false;
	if (InputEvent.type == SDL_MOUSEBUTTONDOWN)
	{
		if (InputEvent.button.button == pressedkey)
		{
			return true;
		}
	}
	return false;
}

bool EInput::GetMousePressed(int pressedkey)
{
	if (InputEvent.type == SDL_MOUSEBUTTONDOWN)
	{
		if (InputEvent.button.button == pressedkey)
		{
			return true;
		}
	}
	return false;
}

bool EInput::GetMouseUp(int pressedkey)
{
	if (pollcount == 0)
		return false;
	if (InputEvent.type == SDL_MOUSEBUTTONUP)
	{
		if (InputEvent.button.button == pressedkey)
		{
			return true;
		}
	}
	return false;
}

bool EInput::GetKeyDown(int pressedkey)
{
	if (pollcount == 0)
		return false;
	if (InputEvent.type == SDL_KEYDOWN)
	{
		if (InputEvent.key.keysym.sym == pressedkey)
		{
			return true;
		}
	}
	return false;
}

bool EInput::GetKeyPress(int pressedkey)
{
	SDL_Scancode key = SDL_GetScancodeFromKey(pressedkey);
	if (currentKeyStates[key])
	{
		return true;
	}
	return false;
}

bool EInput::GetTouchDown()
{
	if (pollcount == 0)
		return false;
	if (InputEvent.type == SDL_FINGERDOWN)
	{
		return true;
	}
	return false;
}

bool EInput::GetTouchMotion()
{
	if(InputEvent.type == SDL_FINGERMOTION)
	{
		return true;
	}
	return false;
}

bool EInput::GetTouchUp()
{
	if (pollcount == 0)
		return false;
	if (InputEvent.type == SDL_FINGERUP)
	{
		return true;
	}
	return false;
}


bool EInput::GetKeyUp(int pressedkey)
{
	if (pollcount == 0)
		return false;
	if (InputEvent.type == SDL_KEYUP)
	{
		if (InputEvent.key.keysym.sym == pressedkey && InputEvent.key.repeat == 0)
		{
			return true;
		}
	}
	return false;
}