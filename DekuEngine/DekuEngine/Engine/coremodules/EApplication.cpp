#include "EApplication.h"

bool EApplication::applicationRunning = false;

bool EApplication::InitApplication(int screenWidth, int screenHeight, int designwidth, int designheight)
{
	//Initialization flag
	bool success = true;
	EUtil::DetectPlatform();
	int currentPlatform = EUtil::GetCurrentPlatform();
	SDL_DisplayMode displayMode;
	if (currentPlatform == EUtil::Platform::Android || currentPlatform == EUtil::Platform::iOS)
	{
		SDL_GetCurrentDisplayMode(0, &displayMode);
		screenWidth = displayMode.w;
		screenHeight = displayMode.h;
	}
	gWindow = new EWindow(screenWidth, screenHeight,designwidth,designheight);
	if (gWindow == NULL)
	{
		EDebug::LogError("Error Loading Window");
	}
	else
	{
		if (gWindow->InitWindow())
		{
			gRenderer = EWindow::GetRenderer();
			//Initialize renderer color
			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

			//Initialize PNG loading
			int imgFlags = IMG_INIT_PNG;
			if (!(IMG_Init(imgFlags) & imgFlags))
			{
				printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
				success = false;
			}

			//Initialize SDL_ttf
			if (TTF_Init() == -1)
			{
				printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
				success = false;
			}

			//Initialize SDL_mixer
			if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
			{
				printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
				success = false;
			}

			//start timer
			timer_App.Start();
		}
	}
	applicationRunning = success;
	return success;
}


bool EApplication::SetActiveSceneTo(EScene* scene)
{
	if (activeScene != nullptr)
	{
		EDebug::LogError("A scene is already active");
		return false;
	}
	activeScene = scene;
	return true;
}

void EApplication::LoadScene()
{
	if (activeScene != nullptr)
	{
		EDebug::LogError("A scene is already active");
		return;
	}
	EScene* scene = new EScene("Deku Scene");
	if (SetActiveSceneTo(scene))
	{
		EDebug::LogWarning("Creating dummy scene because no scene was loaded");
		PlayScene();
	}
}

void EApplication::PlayScene()
{
	if (activeScene != nullptr)
	{
		EDebug::Log("Playing scene: " + activeScene->GetSceneName());
		//activeScene->closeScenePtr = &CloseCurrentScene;//bind(&CloseCurrentScene);
		activeScene->sceneActive = true;
	}
}

bool EApplication::UnloadScene()
{
	if (activeScene != nullptr)
	{
		EDebug::Log("Unloading scene: "+activeScene->GetSceneName());
		delete activeScene;
		activeScene = nullptr;
		return true;
	}
	return false;
}

void EApplication::CloseCurrentScene()
{
	EDebug::LogError("clossed");

}

void EApplication::InputHandler()
{
	//update the input
	EInput::UpdateInput();
	if (activeScene != nullptr)
	{
		activeScene->InputHandler();
	}
}

void EApplication::UpdateHandler()
{
	
	if (activeScene != nullptr)
	{
		activeScene->Update(timer_App.GetDeltaTime());
		if (activeScene->sceneUnload)
		{
			UnloadScene();
		}
	}

}



void EApplication::RenderHandler()
{

	SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 0);
	SDL_RenderClear(gRenderer);

	if (activeScene != nullptr)
	{
		activeScene->Render();
	}
	SDL_RenderPresent(gRenderer);
}

void EApplication::Run()
{
	EUtil::fpsCount = countedFrames / (timer_App.GetTicks() / 1000.f);
	if (EUtil::fpsCount > 2000000)
	{
		EUtil::fpsCount = 0;
	}
	InputHandler();
	UpdateHandler();
	RenderHandler();
	countedFrames++;
}

bool EApplication::IsApplicationRunning()
{
	return applicationRunning;
}

void EApplication::QuitApplication()
{
	applicationRunning = false;
}

void EApplication::Clear()
{
	UnloadScene();

	gRenderer = NULL;

	//Quit SDL subsystems
	Mix_Quit();
	IMG_Quit();
	TTF_Quit();
	//close window
	delete gWindow;
	gWindow = NULL;
}


