#ifndef EACTOR_H
#define EACTOR_H
#include "EGameObject.h"
#include "ETexture.h"
#include "EAnimationController.h"

class EActor : public EGameObject
{
public:
	EActor();
	virtual ~EActor();
	//Set Sprite for the gameObject
	virtual void SetSpriteFromFile(string path);
	//initialize at first tick
	virtual void Initialize() override;
	//update objects 
	virtual void Update(float dt) override;
	//set rendering elements
	virtual void Render() override;
	//check if sprite is set or not
	bool IsSpriteSet();
	//returns animator
	EAnimationController* GetAnimator();
	//returns Texture
	ETexture* GetTexture();
private:

protected:
	ETexture* texture = nullptr;
	EAnimationController* animationController = nullptr;
	void Clear();

};
#endif // !EACTOR_H

