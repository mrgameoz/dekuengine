#include "EAnimationClip.h"



EAnimationClip::EAnimationClip(string pname,int t, int x, int y, int w, int h,bool l)
{
	name = pname;
	totalFrames = t;
	loop = l;
	for (int i = 0; i < t; i++)
	{
		SDL_Rect a;
		a.h = h;
		a.w = w;
		a.x = x;
		a.y = y;
		x += 32;
		sheet.push_back(a);
	}
	currentRect = &sheet[0];
}

EAnimationClip::~EAnimationClip()
{
	Clear();
}

void EAnimationClip::UpdateClip()
{
	if (!CanPlay())
	{
		return;
	}
	SDL_Rect* currentClip;
	if (!loop && clipEnd)
	{
		int c = totalFrames - 1;
		currentClip = &sheet[c];
	}
	else
	{
		currentClip = &sheet[currentFrame / totalFrames];
		//Cycle animation
		++currentFrame;			
		clipEnd = false;
		int val = currentFrame / totalFrames;
		int lastFrame = totalFrames - 1;
		if (!loop && val >= totalFrames)
		{
			clipEnd = true;
		}
		else if (val >= totalFrames)
		{
			clipEnd = true;
			currentFrame = 0;
		}
	}
	currentRect = currentClip;
}

SDL_Rect* EAnimationClip::GetCurrentRect()
{
	return currentRect;
}

void EAnimationClip::SetClipReverse()
{
	if (!clipReverse)
	{
		clipReverse = true;
		reverse(sheet.begin(), sheet.end());
	}
}

void EAnimationClip::ResetClipReverse()
{
	if (clipReverse)
	{
		clipReverse = false;
		reverse(sheet.begin(), sheet.end());
	}
}

bool EAnimationClip::ClipEnded()
{
	return clipEnd;
}

bool EAnimationClip::CompareName(string pName)
{
	return name==pName;
}

void EAnimationClip::ResetClipToStart()
{
	clipEnd = false;
	currentFrame = 0;
	currentRect = &sheet[0];
}

void EAnimationClip::Play()
{
	play = true;
}

void EAnimationClip::Pause(bool pStatus)
{
	pause = pStatus;
}

void EAnimationClip::Stop()
{
	ResetClipToStart();
	stop = true;
}

bool EAnimationClip::CanPlay()
{
	if (play && !pause && !stop)
	{
		return true;
	}
	return false;
}


bool EAnimationClip::IsPlaying()
{
	return play;
}

bool EAnimationClip::IsPaused()
{
	return pause;
}

bool EAnimationClip::IsStopped()
{
	return stop;
}


void EAnimationClip::Clear()
{
	if (currentRect != nullptr)
	{
		currentRect = nullptr;
	}
	sheet.clear();
}