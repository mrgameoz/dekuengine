#include "EAudioMusicComponent.h"


EAudioMusicComponent::EAudioMusicComponent(string path,bool playOnAwake, int vol, bool isALoop,float fadeTime)
{
	clip = Mix_LoadMUS(path.c_str());

	if (clip == nullptr)
	{
		EDebug::LogError("cannot find clip");
	}
	SetVolume(vol);
	SetLoop(isALoop);
	if (playOnAwake)
	{
		Play();
	}
}

EAudioMusicComponent::~EAudioMusicComponent()
{
	Stop();
	Clear();
}

void EAudioMusicComponent::Play()
{
	int loopcount = 1;
	if (IsLooped())
	{
		loopcount = -1;
	}
	Mix_PlayMusic(clip, loopcount);

}

void EAudioMusicComponent::Stop()
{
	/*pause = false;
	Mix_HaltMusic();*/
}

void EAudioMusicComponent::Pause(bool status)
{
	if (status)
		Mix_PauseMusic();
	else
		Mix_ResumeMusic();
	pause = status;
}

void EAudioMusicComponent::SetLoop(bool status)
{
	loop = status;
}

bool EAudioMusicComponent::IsPlaying()
{
	int status =  Mix_PlayingMusic();
	return status==1;
}

bool EAudioMusicComponent::IsPaused()
{
	return pause;
}

bool EAudioMusicComponent::IsLooped()
{
	return loop;
}

void EAudioMusicComponent::SetVolume(int range)
{
	volume = range;
	Mix_VolumeMusic(volume);
}

int EAudioMusicComponent::GetVolume()
{
	return volume;
}

void EAudioMusicComponent::Clear()
{
	if (clip != nullptr)
	{
		Mix_FreeMusic(clip);
	}
}

