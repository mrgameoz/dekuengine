#include "EUtil.h"

float EUtil::fpsCount = 0;
int EUtil::currentPlatform = 0;

float EUtil::GetFPS()
{
	return fpsCount;
}

void EUtil::DetectPlatform()
{
	string nameofPlatform = SDL_GetPlatform();
	if (nameofPlatform == "Windows")
	{
		currentPlatform = 1;
	}
	else if (nameofPlatform == "Mac OS X")
	{
		currentPlatform = 2;
	}
	else if (nameofPlatform == "Linux")
	{
		currentPlatform = 3;
	}
	else if (nameofPlatform == "iOS")
	{
		currentPlatform = 4;
	}
	else if (nameofPlatform == "Android")
	{
		currentPlatform = 5;
	}
	else
	{
		currentPlatform = 0;
	}
}

int EUtil::GetCurrentPlatform()
{	
	return currentPlatform;
}
