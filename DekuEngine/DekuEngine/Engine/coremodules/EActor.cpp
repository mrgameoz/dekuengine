#include "EActor.h"

EActor::EActor()
{
	
}

EActor::~EActor()
{
	Clear();
}

void EActor::Initialize()
{
	EGameObject::Initialize();
}

void EActor::Update(float dt)
{
	EGameObject::Update(dt);
	if (animationController)
	{
		animationController->UpdateClip(dt);
	}
}

void EActor::Render()
{
	EGameObject::Render();
	if (texture == nullptr)
		return;
	if (animationController)
	{
		texture->Render(animationController->GetCurrentClip()->GetCurrentRect(), transform.rotationAngle);
	}
	else
	{
		texture->Render(NULL, transform.rotationAngle);
	}
}

void EActor::SetSpriteFromFile(string path)
{
	if (texture != nullptr)
	{
		delete texture;
		texture = nullptr;
	}
	texture = new ETexture(&transform, path);
}

bool EActor::IsSpriteSet()
{
	return texture != nullptr;
}

EAnimationController* EActor::GetAnimator()
{
	return animationController;
}

ETexture* EActor::GetTexture()
{
	return texture;
}

void EActor::Clear()
{
	if (animationController != nullptr)
	{
		delete animationController;
		animationController = nullptr;
	}
	if (texture != nullptr)
	{
		delete texture;
		texture = nullptr;
	}


}

