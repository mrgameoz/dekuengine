#pragma once
#ifndef ETIMER_H
#define ETIMER_H
#include <SDL.h>

class ETimer
{
public:
	//Initializes variables
	ETimer();

	//start timer
	void Start();
	//stop timer
	void Stop();
	//pause timer
	void Pause(bool status);

	//Gets the timer's ticks
	Uint32 GetTicks();

	//Returns Time Since Start
	Uint32 GetTimeSinceStart();

	//Gets Delta Time
	float GetDeltaTime();

	//is timer started
	bool isStarted();
	//is timer paused
	bool isPaused();

private:
	//The clock time when the timer started
	Uint32 startTicks= 0;

	//The ticks stored when the timer was paused
	Uint32 pausedTicks =0 ;

	//scales time
	float timeScale = 1.0f;

	//used for deltatime
	long currentTime = 0;
	long lastTime = 0;

	bool paused = false;
	bool started = false ;
};



#endif // !ETIMER_H



