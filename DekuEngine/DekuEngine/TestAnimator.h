#include "EngineCore.h"
class TestAnimator : public EAnimationController
{
public:
	TestAnimator(bool playOnAwake);
	~TestAnimator();
	EAnimationClip* idle;
	EAnimationClip* walkL;
	EAnimationClip* walkR;
	EAnimationClip* walkF;
	EAnimationClip* walkB;
	void AnimatorFlow() override;
	enum state
	{
		state_idle,
		state_walkL,
		state_walkR,
		state_walkF,
		state_walkB,
	};
	state animationState;
	//set state
	void SetState(state val);

};

