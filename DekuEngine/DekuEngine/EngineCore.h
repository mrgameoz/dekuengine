#pragma once
#ifndef EngineCore
#define EngineCore
//Core
#include "Engine\coremodules\EDebug.h"
#include "Engine\coremodules\ETexture.h"
#include "Engine\coremodules\EText.h"
#include "Engine\coremodules\EVector.h"
#include "Engine\coremodules\ETransform.h"
#include "Engine\coremodules\EScene.h"
#include "Engine\coremodules\EGameObject.h"
#include "Engine\coremodules\EActor.h"
#include "Engine\coremodules\EAnimationClip.h"
#include "Engine\coremodules\EAnimationController.h"
#include "Engine\coremodules\EAudioMusicComponent.h"
#include "Engine\coremodules\EAudioChunkComponent.h"
#include "Engine\coremodules\ETimer.h"
#include "Engine\coremodules\ECallbackAction.h"
#include "Engine\coremodules\EWindow.h"
#include "Engine\coremodules\EInput.h"
#include "Engine\coremodules\EApplication.h"
//UI
#include "Engine\coremodules\UI\EUIController.h"
#include "Engine\coremodules\UI\EUIComponents.h"
#endif // !EngineCore

