#include "EngineCore.h"
#include "TestPlayer.h"
#include "Engine\coremodules\UI\EUIComponents.h"


class TestScene:public EScene
{
public:
	TestScene(string sceneName);
	~TestScene();
	void Initialize() override;
	void InputHandler() override;
	void Update(float dt) override;
	static Uint32 callbackPointer(Uint32 interval, void* param);
	Uint32 message(Uint32 interval, void* param);
	ECallbackID message_callbackid;

private:
	EActor* dekuimage;
	TestPlayer* player;
	EUIText* fpsText;
	EUIText* helpText;
	EUIButton* exitButton;
	EAudioMusicComponent* bgm;

	float timer = 0;
};

